var route = require('koa-route');
var uuid = require('uuid');

var mongo = require('../utils/mongo-proxy');
var validators = require('../utils/validators');

var USERS_COLL = require('../config/constants').COLLECTIONS.USERS;

module.exports.init = function(app){

    var PATH_REG_EXP = new RegExp('^/api/users/(.*?)/jog-entries', 'i');

    app.use(function* (next){
        var match = PATH_REG_EXP.exec(this.path) || [];
        if(match.length == 2 && match[1] !== this.state.user._id){
            this.status = 401;
        }
        else {
            yield next;
        }
    });

    app.use(route.get('/api/users/:userId/jog-entries/:id', getJogEntry));
    app.use(route.get('/api/users/:userId/jog-entries', getJogEntries));
    app.use(route.post('/api/users/:userId/jog-entries', createJogEntry));
    app.use(route.put('/api/users/:userId/jog-entries/:id', updateJogEntry));
    app.use(route.delete('/api/users/:userId/jog-entries/:id', deleteJogEntry));

};

function *createJogEntry(userId){

    var jog = this.request.body;

    var validation = validators.validateJogEntry(jog);

    if(!validation.valid){
        this.status = 400;
        this.body = validation.errors;
        return;
    }

    jog._id = uuid.v4();

    var res = yield mongo[USERS_COLL]
        .findAndModify(
        {_id : mongo.ObjectID(userId)},
        {},
        {
            $push: {
                jogs: jog
            }
        },
        {
            fields : {}
        }
    );

    if(res[1].ok){
        this.status = 201;
        this.set('location', '/api/users/' + userId + '/jog-entries/' + jog._id);
    }
    else {
        this.status = 500;
    }
}

function *deleteJogEntry(userId, id){

    var res = yield mongo[USERS_COLL]
        .findAndModify(
        {_id : mongo.ObjectID(userId)},
        {},
        {
            $pull: {
                jogs: {_id: id}
            }
        },
        {
            fields : {}
        }
    );

    if(res[1].ok){
        this.status = 200;
    }
    else {
        this.status = 500;
    }

}

function *updateJogEntry(userId, id){

    var jog = this.request.body;

    var validation = validators.validateJogEntry(jog);

    if(!validation.valid){
        this.status = 400;
        this.body = validation.errors;
        return;
    }

    var res = yield mongo[USERS_COLL]
        .update({_id : mongo.ObjectID(userId), 'jogs._id' : id},
        {$set: {'jogs.$' : jog}});


    this.status =   res[0] ? 200 : 404;

}

function *getJogEntry(userId, id){
    var user = yield mongo[USERS_COLL].findOne({_id: mongo.ObjectID(userId)});
    if(user){
        var jog = user.jogs.filter((jog) => jog._id === id)[0];
        if(jog){
            this.status = 200;
            this.body = jog;
            return;
        }

    }
    this.status = 400;

}

function *getJogEntries(userId){
    var user = yield mongo[USERS_COLL].findOne({_id: mongo.ObjectID(userId)});
    if(user){
        var q = this.request.query;
        q.from = q.from || '0000';
        q.to = q.to || '9999';
        this.body = (user.jogs || []).filter((jog)=>{
            return jog.date >= q.from && jog.date <= q.to;
        });
        this.status = this.body.length > 0 ? 200 : 204;
        return;
    }
    this.status = 400;
}