var route = require('koa-route');

var mongo = require('../utils/mongo-proxy');
var USER_COLL = require('../config/constants').COLLECTIONS.USERS;
var ADMIN_ROLE  = require('../config/constants').ROLES.ADMIN;

module.exports.init = function(app){

    app.use(route.get('/api/users', getUsers));
    app.use(route.del('/api/users/:id', deleteUser));

};

function *getUsers(){

    if(this.state.user.role == ADMIN_ROLE){
        var users = yield mongo[USER_COLL].find({}, {email: 1}).toArray();
        users.forEach((u)=> u.jogEntriesUrl = '/api/users/'+u._id + '/jog-entries');
        if(users.length === 0){
            this.status = 204;
        }
        else {
            this.status = 200;
            this.body = users;
        }
    }

}

function *deleteUser(userId){

    if(userId == 'me'){
        userId = this.state.user._id;
    }

    if(userId == this.state.user._id || this.state.user.role == ADMIN_ROLE){
        var res = yield mongo[USER_COLL].remove({_id: mongo.ObjectID(userId)});
        if(res[0] > 0){
            this.status = 200;
        }
        else {
            this.status = 404;
        }
    }

}