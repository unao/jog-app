var route = require('koa-route');
var jwt = require('koa-jwt');
var bcrypt = require('bcrypt');

var mongo = require('../utils/mongo-proxy');
var config = require('../config');

var SECRET = config.SECRET;
var JWT_EXPIRY = config.CONSTANTS.JWT_EXPIRY;
var USERS_COLL = config.CONSTANTS.COLLECTIONS.USERS;

var ERROR_MSG = 'Incorrect credentials.';

module.exports.init = function(app){

    app.use(route.post('/api/login', login));

};

function *login(){

    var ctx = this;
    var credentials = ctx.request.body || {};

    if(credentials.email && credentials.password){
        credentials.email = credentials.email.toLowerCase();
        var user = yield mongo[USERS_COLL].findOne({email: credentials.email});
        if(!user) {
            ctx.status = 400;
            ctx.body = ERROR_MSG;
            return;
        }
        var compare = yield new Promise((resolve, reject)=>{
            bcrypt.compare(credentials.password, user.password, function(err, res){
                if(err){
                    reject(err);
                }
                else {
                    resolve(res);
                }
            });
        });
        if(compare){
            ctx.status = 200;
            delete user.password;
            ctx.body = {
                token : jwt.sign(user, SECRET, JWT_EXPIRY),
                user : user
            };
            return;
        }
    }

    ctx.status = 400;
    ctx.body = ERROR_MSG;

}
