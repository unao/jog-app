var route = require('koa-route');
var jwt = require('koa-jwt');
var fs = require('fs');

var adminRole = require('../config/constants').ROLES.ADMIN;
var config= require('../config');

module.exports.init = function(app){

    app.use(route.post('/api/admin-token', function *(){

        yield new Promise((resolve, reject)=>{

            var token = jwt.sign({role: adminRole}, config.SECRET, 60);
            fs.writeFile(config.adminToken, token, 'utf8', function(err){
                if(err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            });

        });
        this.status = 200;
        this.body = 'Admin token generated.';
    }));

};