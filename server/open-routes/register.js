var bcrypt = require('bcrypt');
var route = require('koa-route');
var co = require('co');

var mongo = require('../utils/mongo-proxy');
var constants = require('../config/constants');
var isEmail = require('../utils/validators').isEmail;
var MIN_PASSWORD_LENGTH = constants.MIN_PASSWORD_LENGTH;

module.exports.init = function (app){

    app.use(route.post('/api/register', createUser));

};

function *createUser(){

    var ctx = this;
    var credentials = ctx.request.body;
    credentials.email = (credentials.email || '').toLowerCase();

    var check = yield checkValidity(ctx, credentials);

    if(check.valid){
        yield insert(ctx, credentials);
    }

}

function *checkValidity(ctx, credentials){

    var errorMessage = '';

    if(!isEmail(credentials.email)){
        ctx.status = 400;
        errorMessage += 'Incorrect email.\n';
    }

    if((credentials.password || '').length < MIN_PASSWORD_LENGTH){
        ctx.status = 400;
        errorMessage += 'Password too short.\n';
    }

    var count = yield mongo[constants.COLLECTIONS.USERS]
        .find({email:credentials.email}).count();

    if(count > 0){
        ctx.status = 409;
        errorMessage += 'User exists';
    }

    if(ctx.status == 400 || ctx.status == 409) {
        ctx.body = errorMessage;
        return {valid: false};
    }

    return {valid: true};
}

function *insert(ctx, credentials){
    credentials.password = yield new Promise(function(resolve, reject){
        bcrypt.hash(credentials.password, 10, function(err, hash){
            if(err){reject(err);}
            resolve(hash);
        });
    });

    credentials.role = constants.ROLES.NORMAL;
    var user = yield mongo[constants.COLLECTIONS.USERS]
        .insert(credentials);
    user = user[0];

    ctx.status = 201;
    ctx.set('location', '/api/user/' + user._id);

}

