var koa = require('koa');
var co = require('co');
var logger = require('koa-logger');
var send = require('koa-send');
var jwt = require('koa-jwt');
var bodyParser = require('koa-bodyparser');
var fs = require('fs');

var config = require('./config');
var mongo = require('./utils/mongo-proxy');

var app = koa();
delete app.env;

module.exports = app;

app.init = co.wrap(function *(testData) {

    yield mongo.init(testData);

    if(config.NODE_ENV !== 'test') {
        app.use(logger());
    }
    app.use(serveStatic);

    app.use(bodyParser());

    initOpenRoutes(app);

    app.use(function *(next){
        try {
            yield next;
        } catch (err) {
            if (401 == err.status) {
                this.status = 401;
                this.body = 'Protected resource, use Authorization header to get access\n';
            } else {
                throw err;
            }
        }
    });
    // middleware below this line is only reached if jwt token is valid
    app.use(jwt({secret: config.SECRET}));

    initApiRoutes(app);

    app.server = app.listen(config.port);
    console.log('Koa server started: ' + config.port);

});

// auto init if this app is not being initialized by another module (i.e. using require('./app').init();)
if (!module.parent) {
    app.init().catch(function (err) {
        console.log('Cannot start the server.');
        console.error(err.stack);
        process.exit(1);
    });
}

function *serveStatic(next){
    // do not handle /api paths
    if (this.path.indexOf('/api/') === 0) {
        yield next;
        return;
    } else if (this.path.indexOf('/public/') === 0) {
        yield send(this, config.publicRoot + this.path.replace('/public', ''));
        return;
    } else {
        // request is for a subdirectory so treat it as an angular route and serve index.html, letting angular handle the routing properly
        yield send(this, config.publicRoot + '/index.html');
    }

}

function initRoutes(app, pathToDir){
    fs.readdirSync(pathToDir).forEach(function(file){
        require(pathToDir+file).init(app);
    });
}

function initOpenRoutes(app){
    initRoutes(app, config.openRoutesRoot);
}

function initApiRoutes(app){
    initRoutes(app, config.apiRoutesRoot);
}
