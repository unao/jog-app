var fs = require('fs');
var faker = require('faker');

var mochaConf = require('./mocha.conf');
var request = mochaConf.request;

var mongo = require('../utils/mongo-proxy');
var config = require('../config');

var rawUser = {
    email : faker.internet.email(),
    password : 'pass123'
};
var user = null;
var jogEntries = mochaConf.jogEntries;
var token = null;

var newJogsLocations = [];

describe('Jog Entries', function () {

    before(function *(){

        yield request.post('/register')
            .send(rawUser)
            .end();

        var res = yield request.post('/login')
            .send(rawUser)
            .end();

        token = 'Bearer ' + res.body.token;
        user = res.body.user;

    });

    it('POST /api/users/:userId/jog-entries should be unauthorized without token', function *(){
        yield request.post('/users/' + user._id + '/jog-entries')
            .send(jogEntries[0])
            .expect(401)
            .end();
    });

    it('POST /api/users/:userId/jog-entries should be unauthorized for different user', function *(){
        yield request.post('/users/' + user._id + 'diff/jog-entries')
            .set('Authorization', token)
            .send(jogEntries[0])
            .expect(401)
            .end();
    });

    it('POST /api/users/:userId/jog-entries should fail for wrong jog-entry', function *(){
        yield request.post('/users/' + user._id + '/jog-entries')
            .set('Authorization', token)
            .send({})
            .expect(400)
            .end();
    });

    it('POST /api/users/:userId/jog-entries should create new records', function *(){
        for(var i in jogEntries){
            var res = yield request.post('/users/'+user._id + '/jog-entries')
                .set('Authorization', token)
                .send(jogEntries[i])
                .expect(201)
                .end();

            res.headers.should.have.property('location');
            newJogsLocations.push(res.headers.location);
        }
    });

    it('GET [jog-entry-location] should return jog-entry', function *(){

        for(var i in newJogsLocations){
            var res = yield request.get(newJogsLocations[i].replace('/api', ''))
                .set('Authorization', token)
                .expect(200)
                .end();

            res.body.should.have.property('distance');
            res.body.should.have.property('time');
            res.body.should.have.property('date');
        }

    });

    it('GET /api/users/:userId/jog-entries should return jog-entries', function *(){

            var res = yield request.get('/users/'+user._id + '/jog-entries')
                .set('Authorization', token)
                .expect(200)
                .end();
            res.body.should.have.lengthOf(newJogsLocations.length);

        }
    );

    it('GET /api/users/:userId/jog-entries?from=date&to=date should select all', function *(){

            var res = yield request.get('/users/'+user._id + '/jog-entries?' +
            'from='+jogEntries[0].date +'&to='+jogEntries[jogEntries.length-1].date )
                .set('Authorization', token)
                .expect(200)
                .end();
            res.body.should.have.lengthOf(newJogsLocations.length);

        }
    );

    it('GET /api/users/:userId/jog-entries?from=date&to=date should select just one', function *(){

            var res = yield request.get('/users/'+user._id + '/jog-entries?' +
            'from='+jogEntries[1].date +'&to='+jogEntries[1].date )
                .set('Authorization', token)
                .expect(200)
                .end();
            res.body.should.have.lengthOf(1);

        }
    );

    it('DELETE [jog-entry-location] should remove jog-entry', function *(){

        for(var i in newJogsLocations){
            yield request.delete(newJogsLocations[i].replace('/api', ''))
                .set('Authorization', token)
                .expect(200)
                .end();
        }

    });

    it('GET /api/users/:userId/jog-entries should be empty', function *(){

            yield request.get('/users/'+user._id + '/jog-entries')
                .set('Authorization', token)
                .expect(204)
                .end();

        }
    );

});