require('co-mocha');
require('should');

var config = require('../config/');
var app = require('../server-main.js');
var jwt = require('koa-jwt');
var fs = require('fs');
var constants = require('../config/constants');
var baseUrl = 'http://localhost:' + config.port + '/api';
var supertest = require('co-supertest');
var request = supertest(baseUrl);

var TEST_DATA = JSON.parse(fs.readFileSync(__dirname + '/test_data.json', 'utf8'));

exports.users = TEST_DATA.users;
exports.jogEntries = TEST_DATA.jogEntries;

exports.request = request;


console.log('Mocha starting to run server tests on port ' + config.port);

before(function *() {
    yield app.init({});
});

// close the server after each test is done
after(function (done) {
    app.server.close(done);
});