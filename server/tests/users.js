var mochaConf = require('./mocha.conf');
var request = mochaConf.request;
var faker = require('faker');


describe('User: create -- login -- delete', function () {

    var userExample = {
        email: faker.internet.email(),
        password: 'pass123'
    };
    var tokenHeader = null;

    it('POST /api/register should fail', function *(){
        yield request // co-supertest goodness, we can yield!
            .post('/register')
            .send({})
            .expect(400)
            .end();
    });

    it('POST /api/register should create a new user', function *() { // co-mocha goodness, we can use generator functions!

        var res = yield request // co-supertest goodness, we can yield!
            .post('/register')
            .send(userExample)
            .expect(201)
            .end();

        res.header.should.have.property('location');

    });

    it('POST /api/register duplicate should fail', function *() { // co-mocha goodness, we can use generator functions!

        yield request // co-supertest goodness, we can yield!
            .post('/register')
            .send(userExample)
            .expect(409)
            .end();

    });

    it('POST /api/login should fail for wrong password', function *(){

        yield request
            .post('/login')
            .send({
                email: userExample.email,
                password: userExample.password + 'xyz'
            })
            .expect(400)
            .end();

    });

    it('POST /api/login should login newly created user', function *(){

        var res = yield request
            .post('/login')
            .send(userExample)
            .expect(200)
            .end();

        res.body.should.have.property('token');
        tokenHeader = 'Bearer ' + res.body.token;
    });

    it('DELETE /api/users/:id should fail when token incorrect/changed', function *(){

        yield request
            .delete('/users/me')
            .set('Authorization', tokenHeader + 'a' )
            .expect(401)
            .end();

    });

    it('DELETE /api/users/:id should fail incorrect user id used', function *(){

        yield request
            .delete('/users/fakeId')
            .set('Authorization', tokenHeader )
            .expect(404)
            .end();

    });

    it('DELETE /api/users/:id should delete previously created', function *(){

        yield request
            .delete('/users/me')
            .set('Authorization', tokenHeader)
            .expect(200)
            .end();

    });
});
