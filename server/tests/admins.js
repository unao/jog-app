var fs = require('fs');

var mochaConf = require('./mocha.conf');
var request = mochaConf.request;

var mongo = require('../utils/mongo-proxy');
var config = require('../config');

var adminToken = null;
var dbUsers = null;
var rawUsers = mochaConf.users;


describe('Admins: create token - delete users', function () {

    before(populateUsers);

    it('POST /admin-token should generate admin token', function *(){

        yield request.post('/admin-token')
            .expect(200)
            .end();

        adminToken = 'Bearer ' + fs.readFileSync(config.adminToken, 'utf8');

    });

    it('GET /users should get all users', function *(){

        var res = yield request.get('/users')
            .set('Authorization', adminToken)
            .expect(200)
            .end();

        res.body.should.have.lengthOf(dbUsers.length);

    });

    it('GET /users should not be accessible with incorrect / missing token', function *(){

        yield request.get('/users')
            .set('Authorization', adminToken+'c')
            .expect(401)
            .end();

        yield request.get('/users')
            .expect(401)
            .end();

    });

    it('DELETE /users/:id should be able to delete all users once', function *(){

        for(var i in dbUsers){
            yield request.delete('/users/' + dbUsers[i]._id)
                .set('Authorization', adminToken)
                .expect(200)
                .end();

            yield request.delete('/users/' + dbUsers[i]._id)
                .set('Authorization', adminToken)
                .expect(404)
                .end();
        }

    });

    it('GET /users should get empty collection', function *(){

        yield request.get('/users')
            .set('Authorization', adminToken)
            .expect(204)
            .end();

    });

});

function *populateUsers(){

    for(var i in rawUsers){
        yield request.post('/register')
            .send(rawUsers[i])
            .end();
    }
    dbUsers = yield mongo.users.find().toArray();

}