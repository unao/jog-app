var self = module.exports = {};

// source: http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
var EMAIL_REGEX = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
self.isEmail = function(aString){
    return EMAIL_REGEX.test(aString);
};


var ENTRY_VALIDATORS = {
    'date' : isDateValid,
    'time' : isTimeValid,
    'distance' : isDistanceValid
}
var JOG_OBLIGATORY_ENTRIES = Object.keys(ENTRY_VALIDATORS);

var DATE_REG_EXP = /\d\d\d\d-\d\d-\d\d/i; // better than nothing

self.validateJogEntry = function(jogEntry){

    var errors = [];

    JOG_OBLIGATORY_ENTRIES.forEach(function(name){
        if(!(name in jogEntry)){
            errors.push('Missing info: ' + name);
        }
        else {
            if(!ENTRY_VALIDATORS[name](jogEntry[name])){
              error.push('Invalid value for ' + name + ': ' + jogEntry[name]);
            };
        }
    });

    if(errors.length > 0){
        return {
            valid : false,
            errors : errors
        };
    }
    else {
        return {
            valid : true
        };
    }
};

function isDateValid(dateString){
    return DATE_REG_EXP.test(dateString);
}

function isTimeValid(time){
    return isFloat(time);
}

function isDistanceValid(distance){
   return isFloat(distance);
}

function isFloat(n){
    try{
        var parsed = parseFloat(n);
        return parsed == n;
    }
    catch(e){
        return false;
    }
}