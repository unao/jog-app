var co = require('co');
var comongo = require('co-mongo');
var config = require('../config');
var constants = require('../config/constants');

var self = module.exports = {};

self.ObjectID = comongo.ObjectID;

self.init = function *init(testData){
    var connect = comongo.connect;

    if (comongo.db) {
        yield comongo.db.close();
    }

    self.db  = yield connect(config.mongoUrl);
    for(var index in constants.COLLECTION_NAMES){
        var name = constants.COLLECTION_NAMES[index];
        self[name] = yield self.db.collection(name);
    }

    if(testData){
        yield resetTestDB(testData);
    }

};

function *resetTestDB(testData){
    if(config.NODE_ENV == 'test'){
        for(var index in constants.COLLECTION_NAMES){
            var name = constants.COLLECTION_NAMES[index];
            try {
                yield self[name].drop();
            }
            catch(err){
                if(err.message !== 'ns not found'){
                    throw err;
                }
                else {
               //     console.log(name + ' not found in ' + config.mongoUrl);
                }
            }
            yield insertToDB(name, testData[name]);
        }
    }
    else {
        throw new Error('Not in test environment.');
    }
}

function *insertToDB(name, data){
    if(!name){
        return;
    }
    for (var k in data){
        yield self[name].insert(data[k]);
    }
}