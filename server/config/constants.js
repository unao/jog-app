module.exports = {

    MIN_PASSWORD_LENGTH : 5,
    JWT_EXPIRY : 24 * 60, // 1 day

    COLLECTIONS: {
        USERS : 'users'
    //    ADMINS : 'admins'
    },

    ROLES : {
        NORMAL : 'normal',
        ADMIN : 'admin'
    }

};

module.exports.COLLECTION_NAMES = Object.keys(module.exports.COLLECTIONS)
    .map(function(k){return module.exports.COLLECTIONS[k];});

Object.freeze(module.exports);