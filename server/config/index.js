var fs = require('fs');

var config = module.exports = {};

config.CONSTANTS = require('./constants');
require('./env.js')(config);
require('./paths.js')(config);
if(fs.existsSync(__dirname + '/confidential.js')){
    require('./confidential.js')(config);
}


