// used as a template for confidential config, which should not be kept in the repo
// just copy it to confidential.js
var argv = require('optimist').argv;

module.exports = function(config){

    config.MONGO_USER = '';
    config.MONGO_PASSWORD = '';

    config.SECRET = argv.SECRET || 'ksdajf2 3u4fdk 809udfa'; // for signing jwt tokens

};

