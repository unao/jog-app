var path = require('path');

module.exports = function(config){
    config.root = path.normalize(__dirname + '/../../');
    config.publicRoot = config.root + 'public';
    config.openRoutesRoot = config.root + 'server/open-routes/';
    config.apiRoutesRoot = config.root + 'server/private-routes/';
    config.adminToken = config.root + 'server/config/admin-token.'+config.NODE_ENV;
};