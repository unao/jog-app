var argv = require('optimist').argv;

module.exports = function(config) {

    config.NODE_ENV = argv.NODE_ENV || process.env.NODE_ENV;
    if (!config.NODE_ENV) {
        console.error('NODE_ENV not found. Too risky, if in production.');
        process.exit(1);
    }

    if (config.NODE_ENV == 'production') {
        console.error('`production` not configured.');
        process.exit(1);
    }
    else if (config.NODE_ENV == 'dev') {

        config.port = argv.port || 9111;
        config.mongoUrl = 'mongodb://127.0.0.1:27017/ttJog';

    }
    else if (config.NODE_ENV == 'test') {
        config.port = argv.port || 9119;
        config.mongoUrl = 'mongodb://127.0.0.1:27017/ttJogTest';
    }
    else {
        console.error('Unknown environment: ' + config.NODE_ENV);
        process.exit(1);
    }

};