module.exports = function(app){

    app.config(require('./routes.js'));
    app.config(require('./authInterceptor.js'));

}