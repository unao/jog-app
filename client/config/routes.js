module.exports = function($stateProvider, $urlRouterProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider
        .state('dashboard', {
            url: '/',
            template: '<tt-jog-dashboard></tt-jog-dashboard>',
            onEnter : function($state, auth){
                console.log(auth.isLoggedIn());
                if(!auth.isLoggedIn()){
                    $state.go('login');
                }
            }
        })
        .state('login', {
            url: '/login',
            template: '<tt-jog-login></tt-jog-login>',
            onEnter : function($state, auth){
                if(auth.isLoggedIn()){
                    $state.go('dashboard');
                }
            }
        })
        .state('admin', {
           url: '/admin',
            template: '<tt-jog-admin></tt-jog-admin>'
        });


    $urlRouterProvider.otherwise('/');

}