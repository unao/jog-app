module.exports = function($httpProvider){
    $httpProvider.interceptors.push(authInterceptor);
}

function authInterceptor($q, $window, tokenAndUser) {
    return {
        request: function (config) {
            var token = tokenAndUser.getToken();
            if (token) {
                config.headers['Authorization'] = 'Bearer ' + token;
            }
            return config;
        },

        response: function (response) {
            if (response.status === 401) {
                console.log("Response 401");
            }
            return response || $q.when(response);
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                tokenAndUser.clear();
    //            $window.location.reload();
            }
            return $q.reject(rejection);
        }
    }
}