module.exports = function(jog){

    jog.date && (jog.dateDisplay = jog.date.split('T')[0]);
    jog.average = Math.round(jog.distance / (jog.time / 3600)*100)/100; // rounded to 2 places
    var time = jog.time;
    var timeDisplay = [];
    while(time){
        var mod = time%60;
        time = (time - mod)/60;
        mod = mod < 10 ? '0' + mod : mod + '';
        timeDisplay.unshift(mod);
    }
    while(timeDisplay.length < 3){
        timeDisplay.unshift('00');
    }
    jog.timeDisplay = timeDisplay.join(':');

}