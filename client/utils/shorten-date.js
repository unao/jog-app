module.exports = function(date){

    var month = adjustNumber(date.getMonth()+1);
    var day = adjustNumber(date.getDate());


    return date.getFullYear()+'-'+ month +  '-' + day;

}

function adjustNumber(number){
    if(number < 10){
        return '0' + number;
    }
    else {
        return '' + number;
    }
}