module.exports = function(app){

    app.value('jogFormatter', require('./jog-formatter'));
    app.value('shortenDate', require('./shorten-date'));

}