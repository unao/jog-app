module.exports = function($http, jogFormatter, tokenAndUser){

    var self = this;

    self.createJogEntry = function(jogEntry){
        console.log('Creating jog: ', jogEntry);
        return $http.post('/api/users/' + tokenAndUser.getId() + '/jog-entries', jogEntry);
    };

    self.updateJogEntry = function(jogEntry){
        console.log('Updating jog: ', jogEntry);

        return $http.put('/api/users/' +
        tokenAndUser.getId() + '/jog-entries/' + jogEntry._id, jogEntry);
    };

    self.removeJogEntry = function(jogId){
        return $http.delete('/api/users/' + tokenAndUser.getId() + '/jog-entries/' + jogId);
    };

    self.getJogEntries = function(){
        console.log('ID', tokenAndUser.getId());
        return $http.get('/api/users/' + tokenAndUser.getId() + '/jog-entries')
            .then(function(res){
                if(res.status == 200){
                    res.data.forEach(jogFormatter);
                    return res.data;
                }
                else if(res.status == 204){
                    return [];
                }
                else {
                    console.error(res);
                    throw Error('Unknown error', res);
                }
            })
            .then(function(entries){
                return entries.sort(function(e1, e2){
                    return e1.date < e2.date;
                })
            })
    };

    return self;
}
