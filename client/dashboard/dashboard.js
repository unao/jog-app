module.exports = function(){

    return {
        template : require('./dashboard.tmp.html'),
        controller : dashboardController,
        controllerAs : 'dash'
    }

}

function dashboardController($modal, auth,
                             jogProxy,  jogFormatter,
                             shortenDate, jogManager){

    var dash = this;

    init();

    dash.refresh = refresh;
    dash.createJog  = createJog;
    dash.removeJog = removeJog;
    dash.updateJog = updateJog;
    dash.filter = filterEntries;

    dash.showWeeksSummary = function(){
        $modal.open({
            template : require('./weeks-summary.tmp.html'),
            controller : 'weeksSummaryCtrl',
            controllerAs : 'modal',
            resolve : {
                jogEntries : function(){
                    return dash.filteredEntries
                }
            }
        });
    }

    function refresh(){
        startLoading();
        console.log('Refreshing dash');
        jogManager.getJogEntries()
            .then(function(entries){
                console.log('entries-dashboard', entries);
                dash.jogEntries = entries;
            })
            .catch(function(){
                alert('Cannot get entries from server');
            })
            .finally(function(){
                stopLoading();
            })
    }

    function createJog(){
        return jogProxy.createJog()
            .then(dash.refresh)
            .catch(function(){
                alert('Could not create the jog entry.')
            });
    }

    function removeJog(jog){

        var index = dash.jogEntries.indexOf(jog);
        console.log('jog index', index);
        if(index > -1){
            dash.jogEntries.splice(index, 1);
        }
        return jogManager.removeJogEntry(jog._id)
            .then(function(){
                console.log('removed', jog);
            })
            .catch(function(){
                alert('cannot remove jog entry');
                dash.refresh();
            })

    }

    function updateJog(jogEntry){

        return jogProxy.updateJog(jogEntry)
            .then(dash.refresh)
            .catch(function(){
                alert('Could not update the jog entry.')
            });

    }

    function filterEntries(jog, index, arr){
        if(index === 0){
            dash.total = {
                time : 0,
                distance : 0
            }
            dash.fromDate = (dash.dateFilter.from && shortenDate(dash.dateFilter.from)) || '0000';
            dash.toDate = (dash.dateFilter.to && shortenDate(dash.dateFilter.to)) || '9999';
        }

        var result = jog.date >= dash.fromDate && jog.date <= dash.toDate;

        if(result) {
            dash.total.time += jog.time;
            dash.total.distance += jog.distance;
        }

        if(index === arr.length-1){
            jogFormatter(dash.total);
        }


        return result;
    }

    // private

    function init(){

        dash.auth = auth;
        dash.user = auth.user;

        dash.showEntries = true;

        dash.dateFilter = {
            from : null,
            to: null
        }

        refresh();

    }

    var loadingCount = 0;
    function startLoading(){
        loadingCount += 1;
        dash.loading = true;
    }

    function stopLoading(){
        loadingCount -= 1;
        dash.loading = loadingCount == 0;
    }

}