module.exports = function($modalInstance, jogFormatter,
                          jogEntries, shortenDate){

    var self = this;

    init();

    self.close = function(){
        $modalInstance.close();
    }

    function init(){

        var weeks = {};

        jogEntries.forEach(appendWeekStart);
        jogEntries.forEach(addToBucket);

        self.summary = getSummary();

        console.log(weeks, self.summary);

        function getSummary(){

            return Object.keys(weeks)
                .map(function(key){
                    jogFormatter(weeks[key]);
                    return weeks[key];
                })

        }

        function appendWeekStart(jog){

            var date = new Date(jog.date);
            jog.weekStart = new Date(date.setDate((date.getDate()-date.getDay())));


        }

        function addToBucket(jog){

            if(!(jog.weekStart in weeks)){
                weeks[jog.weekStart] = {
                    fullDate : jog.weekStart,
                    date : shortenDate(jog.weekStart),
                    time : 0,
                    distance : 0
                }
            }

            weeks[jog.weekStart].time += jog.time;
            weeks[jog.weekStart].distance += jog.distance;

        }
    }



}