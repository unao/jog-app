module.exports = function(app){

    app.directive('ttJogDashboard', require('./dashboard.js'));
    app.directive('ttJogDateSelection', require('./date-selection.js'));
    app.controller('weeksSummaryCtrl', require('./weeks-summary-ctrl'));

};