module.exports = function(){


    return {
        template : require('./login.tmp.html'),
        controller : loginCtrl,
        controllerAs: 'login'
    }

};

function loginCtrl($timeout, $state, auth){

    var login = this;

    login.user = {};

    login.register = function(){

        login.loading = true;
        var p = auth.register(login.user);
        handleAfterPost(p);

    };

    login.logIn = function(){
        login.loading = true;
        var p = auth.logIn(login.user);
        handleAfterPost(p);
    }

    login.credOK = function(){
        return login.user.email && login.user.password;
    }

    login.clearError = function(){
        login.error = '';
    }

    login.goToAdmin = function(){
        $state.go('admin');
    };

    function handleAfterPost(postPromise){

        return postPromise
            .catch(function(err){
                login.error = err.data;
                $timeout(login.clearError, 5*1000);
            })
            .finally(function(){
                login.loading = false;
            })

    }

}