module.exports = function(app){

    app.controller('jogCreateEditCtrl', require('./jog-create-edit-modal-ctrl'));
    app.directive('ttJogJogCreateEdit', require('./jog-create-edit-dir'));
    app.factory('jogProxy', require('./jog-proxy'));

};