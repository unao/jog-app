module.exports = function($modal, $q, jogManager){

    var self = this;

    self.createJog = createJog;
    self.updateJog =  updateJog;

    return self;

    function createJog(){
        return showDialog({});
    };

    function updateJog(jog){
        return showDialog(jog);
    };

    // private

    function showDialog(jog){

        var modalInstance = $modal.open({
            template : require('./jog-create-edit-modal.tmp.html'),
            controller : 'jogCreateEditCtrl',
            controllerAs: 'modal',
            backdrop : 'static',
            keyboard : 'false',
            resolve : {
                jog : function(){
                    return jog;
                }
            }
        })

        return modalInstance.result
            .then(function(jogEntry){
                if(jogEntry._id){
                    return jogManager.updateJogEntry(jogEntry);
                }
                else {
                    return jogManager.createJogEntry(jogEntry);
                }
            })
            .catch(function(err){
                if(err == 'cancel'){
                    return $q.when('cancel');
                }
                else {
                    return $q.reject(err);
                }
            });

    }

}