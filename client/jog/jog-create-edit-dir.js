module.exports = function(){



    return {
        template : require('./jog-create-edit.tmp.html'),
        controller : jogCreateController,
        controllerAs : 'jog',
        scope : {
            onDone : '&',
            jogEntry : '='
        }
    }

};

function jogCreateController($scope, shortenDate){

    var jog = this;

    init();

    jog.createOrEdit = function(){

        if($scope.form && $scope.form.$valid){

            jog.loading = true;

            $scope.onDone({jog:getProperData()});

            //jogManager.createJogEntry(getProperData())
            //    .then(function(res){
            //        console.log('CREATE', res);
            //        $scope.onCreated();
            //        jog.clear();
            //    })
            //    .catch(function(){
            //        alert('Cannot add new entry');
            //    })
            //    .finally(function(){
            //        jog.loading = false;
            //    })
        }

    };

    jog.clear = clear;

    function clear(){

        $scope.form && $scope.form.$setPristine();
        jog.date = null;
        jog.time = null;
        jog.distance = null;
        jog.datepicker = {
            format: 'yyyy-MM-dd',
            open: false,
            handleOpen: function () {
                console.log('datepicker-open')
                jog.datepicker.open = !jog.datepicker.open;
            }
        };
    }

    // private

    function init(){

        clear();

        if($scope.jogEntry){
            angular.extend(jog, $scope.jogEntry);
            console.log('jog', jog);
        }

        jog.ngModelOpt = { updateOn: 'default blur', debounce: { default: 500, blur: 0 } };

        console.log(jog);

    }

    function getProperData(){

        return {
            date : typeof jog.date === 'string' ? jog.date : shortenDate(jog.date),
            time : getProperTime(),
            distance : parseFloat(jog.distance),
            _id : jog._id
        };

        function getProperTime(){

            return jog.time.split(':')
                .reverse()
                .reduce(function(acc, curr){

                    acc.time += acc.multiplier * parseInt(curr);
                    acc.multiplier *=60;

                    return acc;
                }, {
                    time: 0,
                    multiplier: 1
                }).time;

        }


    }

}