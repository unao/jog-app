module.exports = function($scope, $modalInstance, jog){

    var self = this;

    self.jog = {
        _id : jog._id,
        time : jog.timeDisplay,
        date : jog.dateDisplay,
        distance : jog.distance
    };
    self.headerTxt = jog._id  ? 'Edit' : 'Create';

    self.cancel = function(){
        $modalInstance.dismiss('cancel');
    };

    self.done = function(jogEntry){
        console.log('DONE', jogEntry);
        $modalInstance.close(jogEntry);
    }

    console.log('modal-scope', $scope);

}