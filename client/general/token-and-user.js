module.exports = function($window){

    var self = {};

    var token,
        user;

    var EMPTY_CRED = {
        user : null,
        token : null
    };
    var STORAGE_TAG = 'ttJog-cred';

    init();

    self.saveTokenAndUser = function(data){
        token = data.token;
        user = data.user;
        console.log('saving', data, JSON.stringify(data));
        $window.localStorage.setItem(STORAGE_TAG, JSON.stringify(data));
    };

    self.getToken = function (){
        return token;
    };

    self.getUser = function(){
        return user;
    };

    self.getId = function(){
        var user = self.getUser();
        return user && user._id;
    }

    self.clear = function(){
        self.saveTokenAndUser(EMPTY_CRED);
    };

    return self;

    // private

    function init(){
        var cred = EMPTY_CRED;

        try {
            cred = JSON.parse($window.localStorage.getItem(STORAGE_TAG));
        }
        catch(ex){
            console.error(ex);
        }

        token = cred.token;
        user = cred.user;
    }

}