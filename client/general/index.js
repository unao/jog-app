module.exports = function(app){

    app.factory('tokenAndUser', require('./token-and-user.js'));
    app.factory('auth', require('./auth.js'));

}