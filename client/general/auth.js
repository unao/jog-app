module.exports = function($http, $state, tokenAndUser){

    var auth = this;

    Object.defineProperty(auth, 'user', {
       get: function(){
           return tokenAndUser.getUser();
       }
    });

    auth.isLoggedIn = function(){
        var token = tokenAndUser.getToken();
        return !!token;
    };

    auth.logIn = function(user){
        return $http.post('/api/login', user)
            .then(function(res){
                tokenAndUser.saveTokenAndUser(res.data);
                $state.go('dashboard');
            });
    };

    auth.logOut = function(){
        tokenAndUser.clear();
        $state.go('login');
    };

    auth.register = function(user){
        return $http.post('/api/register', user)
            .then(function(){
                return auth.logIn(user);
            });
    };


    return auth;

};
