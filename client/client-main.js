var app = angular.module('ttJog', ['ui.router', 'ui.bootstrap']);

require('./config')(app);
require('./utils')(app);
require('./general')(app);
require('./model')(app);
require('./admin')(app);
require('./login')(app);
require('./dashboard')(app);
require('./jog')(app);
