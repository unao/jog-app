module.exports = function(){


    return {
        template : require('./admin.tmp.html'),
        controller : adminCtrl,
        controllerAs : 'admin'
    }

}

function adminCtrl($http){

    var admin = this;

    var token = null;

    admin.generateToken = function(){
        $http.post('/api/admin-token')
            .then(function(){
                alert('token generated');
            })
            .catch(function(){
                alert('cannot generate token');
            });
    };

    admin.setToken = function(){
        token = 'Bearer ' + admin.tokenTemp;
        admin.tokenTemp = null;
        return admin.refreshUsers();
    };

    admin.isTokenSet = function(){
        return !!token;
    }

    admin.refreshUsers = function(){

        admin.users = [];
        return $http({
            method: 'GET',
            url: '/api/users',
            headers: {
                'Authorization' : token
            }
        })
            .then(function(res){
                admin.users = res.data;
                console.log(admin.users);
            })
            .catch(function(err){
                alert('cannot get users');
            });

    };

    admin.removeUser = function(user){
        console.log('deleting user', user);
        return $http({
            method: 'DELETE',
            url: '/api/users/' + user._id,
            headers: {
                'Authorization' : token
            }
        })
            .catch(function(){
                alert('cannot delete: ' + user._id);
            })
            .finally(admin.refreshUsers);
    }

}