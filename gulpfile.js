'use strict';

var watchify = require('watchify');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var jshint = require('gulp-jshint');
var merge = require('merge-stream');
var assign = require('lodash.assign');

var SERVER_GLOB = './server/**/*.js';

var CLIENT_PATH = './client/';
var DIST_PATH = './public/';
var FILES_NAMES = ['index.html', 'styles.css', 'assets/**/*'];

var FILES_NAMES_TO_CLIENT = addPrefix(FILES_NAMES, CLIENT_PATH);
function addPrefix(source, prefix){
    var result = {};
    source.forEach(function(name){
        result[name] = prefix + name;
    })
    return result;
}

gulp.task('watch-files', function(){
    FILES_NAMES.forEach(function(name){
        var source = FILES_NAMES_TO_CLIENT[name];
        gulp.src(source).pipe(gulp.dest(DIST_PATH));
        gulp.watch(source, function(){
            gulp.src(source).pipe(gulp.dest(DIST_PATH));
        })
    })
});

gulp.task('client-js', ['watch-files'], function(){

    var customOpts = {
        entries: ['./client/client-main.js'],
        debug: true
    };
    var opts = assign({}, watchify.args, customOpts);
    var b = watchify(browserify(opts));

    b.on('update', bundle);
    b.on('log', gutil.log);

    function bundle(changedFiles) {
        var compileStream = b.bundle()
            .on('error', gutil.log.bind(gutil, gutil.colors.red('Browserify Error\n')))
            .pipe(source('app.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(DIST_PATH));

        if(changedFiles){
            var lintStream = gulp.src(changedFiles)
                .pipe(jshint())
                .pipe(jshint.reporter('default'))

            return merge(lintStream, compileStream);
        }
        else {
            return compileStream;
        }
    }

    return bundle();

});

gulp.task('server-js', function(){

    gulp.watch(SERVER_GLOB, function(){
        gutil.log('Server files changed');
        return gulp.src('./server/**/*.js')
            .pipe(jshint())
            .pipe(jshint.reporter('default'));
    })

});

gulp.task('default', ['client-js', 'server-js']);


